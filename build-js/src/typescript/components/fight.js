import { controls } from '../../constants/controls';
import { createElement } from "../helpers/domHelper";
export async function fight(firstFighter, secondFighter) {
    let fightInfo = {
        firstFighter: {
            fighterObj: firstFighter,
            player: '1',
            healthIndicator: document.getElementById("left-fighter-indicator"),
            health: firstFighter.health,
            block: false,
            orderToAttack: true,
            orderToCriticalHit: false,
            critTimer: 10
        },
        secondFighter: {
            fighterObj: secondFighter,
            player: "2",
            healthIndicator: document.getElementById("right-fighter-indicator"),
            health: secondFighter.health,
            block: false,
            orderToAttack: true,
            orderToCriticalHit: false,
            critTimer: 10
        }
    };
    const CritHitTimeout = 10000;
    const critIndecators = createElement({
        tagName: 'div',
        className: 'fight-critIndicators'
    });
    const critIndecatorFirst = createElement({
        tagName: 'div',
        className: 'fight-critIndicator_first fight-critIndicator',
        attributes: { id: "critIndicator_first" }
    });
    const critIndecatorSecond = createElement({
        tagName: 'div',
        className: 'fight-critIndicator_second fight-critIndicator',
        attributes: { id: "critIndicator_second" }
    });
    const root = document.getElementById('root');
    critIndecators.appendChild(critIndecatorFirst);
    critIndecators.appendChild(critIndecatorSecond);
    root.appendChild(critIndecators);
    const firstIndicator = document.getElementById("critIndicator_first");
    const secondIndicator = document.getElementById("critIndicator_second");
    const rightFighter = document.getElementsByClassName("arena___right-fighter");
    function changeCritTime(fighter, indicator) {
        setTimeout(function tick() {
            if (!fighter.orderToCriticalHit) {
                fighter.critTimer -= 1;
                indicator.innerHTML = fighter.critTimer.toString();
                setTimeout(tick, 1000);
            }
            else {
                fighter.critTimer = 10;
                indicator.innerHTML = "READY";
            }
        }, 1000);
    }
    function attackMove(fighter, movpxf, movpxs) {
        let elem = document.getElementsByClassName(`arena___${fighter}-fighter`)[0];
        elem.style.transform = `translateX(${movpxf}px)`;
        setTimeout(() => {
            elem.style.transform = `translateX(${movpxs}px)`;
        }, 200);
    }
    function critMove(fighter) {
        let elem = document.getElementsByClassName(`arena___${fighter}-fighter`)[0];
        elem.classList.add(`move-${fighter}`);
        setTimeout(() => {
            elem.classList.remove(`move-${fighter}`);
        }, 300);
    }
    function attackBlockMove(attacker, defender, movpxf, movpxs) {
        let attackerElem = document.getElementsByClassName(`arena___${attacker}-fighter`)[0];
        let defenderElem = document.getElementsByClassName(`arena___${defender}-fighter`)[0];
        attackerElem.style.transform = `translateX(${movpxf}px)`;
        defenderElem.style.transform = `translateX(${movpxs}px)`;
        setTimeout(() => {
            attackerElem.style.transform = `translateX(${0}px)`;
            defenderElem.style.transform = `translateX(${0}px)`;
        }, 200);
    }
    changeCritTime(fightInfo.firstFighter, firstIndicator);
    changeCritTime(fightInfo.secondFighter, secondIndicator);
    return new Promise((resolve) => {
        setTimeout(() => {
            fightInfo.firstFighter.orderToCriticalHit = true;
            fightInfo.secondFighter.orderToCriticalHit = true;
        }, CritHitTimeout);
        alert("FIGHT");
        function checkHealth(healthFirst, healthSecond) {
            if (healthFirst <= 0) {
                fightInfo.firstFighter.healthIndicator.style.width = `0%`;
                resolve(secondFighter);
            }
            else if (healthSecond <= 0) {
                fightInfo.secondFighter.healthIndicator.style.width = `0%`;
                resolve(firstFighter);
            }
        }
        function changeHealthIndicator(indicator, health, fighter) {
            indicator.style.width = `${health / fighter.health * 100}%`;
        }
        function checkPressed(buttons, mustBePrsessed) {
            for (let code of mustBePrsessed) {
                if (!buttons.has(code)) {
                    return false;
                }
            }
            return true;
        }
        function checkAttackOrder(fighter) {
            if (fighter.block) {
                return false;
            }
            else {
                return true;
            }
        }
        function critHit(attacker, defender) {
            attacker.orderToCriticalHit = false;
            defender.health -= attacker.fighterObj.attack * 2;
            changeHealthIndicator(defender.healthIndicator, defender.health, defender.fighterObj);
            setTimeout(() => { attacker.orderToCriticalHit = true; }, CritHitTimeout);
        }
        function attack(attacker, defender) {
            defender.health -= getDamage(attacker.fighterObj, defender.fighterObj);
            changeHealthIndicator(defender.healthIndicator, defender.health, defender.fighterObj);
        }
        let pressed = new Set();
        document.addEventListener('keydown', function keyPress(event) {
            pressed.add(event.code);
            if (pressed.has(controls.PlayerOneBlock)) {
                fightInfo.firstFighter.block = true;
            }
            else {
                fightInfo.firstFighter.block = false;
            }
            if (pressed.has(controls.PlayerTwoBlock)) {
                fightInfo.secondFighter.block = true;
            }
            else {
                fightInfo.secondFighter.block = false;
            }
            const pressedD = pressed.has(controls.PlayerOneBlock);
            const pressedJ = checkPressed(pressed, [controls.PlayerTwoAttack]);
            const pressedL = pressed.has(controls.PlayerTwoBlock);
            const pressedA = checkPressed(pressed, [controls.PlayerOneAttack]);
            const pressedQWE = checkPressed(pressed, controls.PlayerOneCriticalHitCombination);
            const orderToQWE = fightInfo.firstFighter.orderToCriticalHit;
            const pressedUIO = checkPressed(pressed, controls.PlayerTwoCriticalHitCombination);
            const orderToUIO = fightInfo.secondFighter.orderToCriticalHit;
            const orderToA = checkAttackOrder(fightInfo.firstFighter);
            const orderToJ = checkAttackOrder(fightInfo.secondFighter);
            if (pressedD && pressedJ) {
                pressed.delete(controls.PlayerTwoAttack);
                attackBlockMove("right", "left", -350, -100);
            }
            else if (pressedA && pressedL) {
                pressed.delete(controls.PlayerOneAttack);
                attackBlockMove("left", "right", 350, 100);
            }
            else if (pressedQWE && orderToQWE) {
                critHit(fightInfo.firstFighter, fightInfo.secondFighter);
                changeCritTime(fightInfo.firstFighter, firstIndicator);
                critMove("left");
            }
            else if (pressedUIO && orderToUIO) {
                critHit(fightInfo.secondFighter, fightInfo.firstFighter);
                changeCritTime(fightInfo.secondFighter, secondIndicator);
                critMove("right");
            }
            else if (pressedA && orderToA) {
                attack(fightInfo.firstFighter, fightInfo.secondFighter);
                pressed.delete(controls.PlayerOneAttack);
                attackMove("left", 350, 0);
            }
            else if (pressedJ && orderToJ) {
                attack(fightInfo.secondFighter, fightInfo.firstFighter);
                pressed.delete(controls.PlayerTwoAttack);
                attackMove("right", -350, 0);
            }
            checkHealth(fightInfo.firstFighter.health, fightInfo.secondFighter.health);
        });
        document.addEventListener('keyup', function keyUnPress(event) {
            pressed.delete(event.code);
        });
    });
}
export function getDamage(attacker, defender) {
    let attack = getHitPower(attacker);
    let block = getBlockPower(defender);
    let damage = attack - block;
    return Math.max(0, damage);
}
export function getHitPower(fighter) {
    const criticalHitChance = (min, max) => {
        return Math.random() * (max - min) + min;
    };
    const power = fighter.attack * criticalHitChance(1, 2);
    return power;
}
export function getBlockPower(fighter) {
    const dodgeChance = (min, max) => {
        return Math.random() * (max - min) + min;
    };
    const power = fighter.defense * dodgeChance(1, 2);
    return power;
}
