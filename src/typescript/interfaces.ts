export interface FightersType {
    [index: number]: {
        _id: string,
        name: string,
        source: string
    };
}
export interface FighterDetailsType {
    _id: string,
    name: string,
    health: number,
    attack: number,
    defense: number,
    source: string,
    [param: string]: string | number;
}
export interface FightersDetailsType extends Array<FighterDetailsType> { }
export interface CustomFighterInfo {
    fighterObj: FighterDetailsType,
    player: string,
    healthIndicator: HTMLDivElement,
    health: number,
    block: boolean,
    orderToAttack: boolean,
    orderToCriticalHit: boolean,
    critTimer: number
}
export interface CustomFightersInfo {
    [key: string]: CustomFighterInfo
}
export interface ModalChild{
    title: string,
    bodyElement: HTMLElement,
    onClose?: Function
}
interface Atributes{
    id?: string,
    src?: string,
    name?: string,
    title?:string,
    alt?:string,
    [key: string]: string | undefined;
}
export interface DOMElem{
    tagName: string,
    className?: string,
    attributes?: Atributes
}