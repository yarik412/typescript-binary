import { createElement } from '../helpers/domHelper';
import { FighterDetailsType } from '../interfaces';

export function createFighterPreview(fighter: FighterDetailsType, position: string): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const infoBlock: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighter-preview-info__root'
  });
  const infoList: HTMLElement = createElement({
    tagName: 'ul',
    className: 'fighter-preview-info__list'
  });
  const placeForImg: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighter-preview-info_imgPlace'
  });
  const selectedInfo: string[] = ["Name", "Health", "Attack", "Defense"]

  try{  
    placeForImg.append(createFighterImage(fighter));
    fighterElement.append(placeForImg);
    createInfoBlock(selectedInfo, infoList, fighter)
    infoBlock.append(infoList);
    fighterElement.append(infoBlock);
  }catch(err){
    null
  }
  return fighterElement;
}

function createInfoBlock(selected: string[],infoList: HTMLElement, fighter: FighterDetailsType): void{
  selected.forEach((param: string) => {
    const info: HTMLElement = createElement({
      tagName: 'li',
      className: 'fighter-preview-info__elem'
    });

    info.innerHTML = param + ": " + fighter[param.toLowerCase()];
    infoList.appendChild(info);
  });
}

export function createFighterImage(fighter: FighterDetailsType): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement: HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
