import { showModal } from './modal'
import { createFighterImage } from '../fighterPreview'
import { createElement } from '../../helpers/domHelper'
import { FighterDetailsType } from '../../interfaces';

export function showWinnerModal(fighter: FighterDetailsType): void {
  const title: string = `Winner: ${fighter.name}`
  const bodyElement: HTMLElement = createElement({
    tagName: 'div',
    className: 'modal-body'
  })
  const img: HTMLElement = createFighterImage(fighter)
  bodyElement.append(img)
  showModal({ title, bodyElement })
}
