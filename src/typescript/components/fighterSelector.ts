import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import {FighterService} from '../services/fightersService'
import { FightersDetailsType, FighterDetailsType } from '../interfaces';
import versusImg from '../../../resources/versus.png';

export function createFightersSelector() {
  let selectedFighters: FightersDetailsType = [];

  return async (event: Event, fighterId: string) => {
    const fighter: FighterDetailsType = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId: string) {
  if(!fighterDetailsMap.has(fighterId)) {
    const newFighterService = new FighterService();
    const fighter = newFighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighter);
    return fighter
  }else{
    return fighterDetailsMap.get(fighterId);
  }
}

function renderSelectedFighters(selectedFighters: FightersDetailsType) {
  const fightersPreview = <HTMLDivElement> document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview: HTMLElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLElement = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: FightersDetailsType) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: FightersDetailsType) {
  renderArena(selectedFighters);
}
