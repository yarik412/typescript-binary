import { callApi } from '../helpers/apiHelper';

export class FighterService {
  async getFighters() {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: any = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string) {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const fighterDetails = await callApi(endpoint, 'GET');

      return fighterDetails;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
